/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacionfuncional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import jdk.nashorn.internal.objects.NativeArray;

/**
 *
 * @author to
 */
public class ProgramacionFuncional {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int a = 2, b = 7, c = 0;
        List<Object> list = new ArrayList<Object>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        System.out.println("factorial(" + b + "):" + factorial(b));
        System.out.print("invertirLista(");
        for (Object obj : list) {
            System.out.print(" " + obj);
        }
        System.out.print("):");
        list = invertirLista(list);
        for (Object obj : list) {
            System.out.print(" " + obj);
        }
        System.out.println();
        System.out.println("fibonacci(" + b + "):" + fibonacci(b));
        System.out.println("sumaNaturales(" + b + "):" + sumaNaturales(b));
        System.out.print("mprimirNaturales(" + a + "..." + b + "):");
        imprimirNaturales(a, b);
        System.out.println();
        System.out.println("contarDigitos(" + c + "):" + contarDigitos(c));
        System.out.println("XeYMultiplicaciones(" + a + "^" + b + "):" + XeYMultiplicaciones(a, b));
        System.out.println("XxYsumas(" + a + "*" + b + "):" + XxYsumas(a, b));
    }

    public static int factorial(int n) {//1
        if (n == 0) {
            return 1;
        }
        if (n > 0) {
            return n * factorial(n - 1);
        }
        return 0;//Caso excepcional cuando n es 0 o menor desde el inicio de la ejecución
    }

    public static List<Object> invertirLista(List<Object> lista) {//2
        if (lista == null) {
            return null;
        }
        List<Object> result = new ArrayList<Object>();
        if (lista.size() == 1) {

            return lista;
        } else {
            Object obj = lista.get(0);
            lista.remove(0);
            result = invertirLista(lista);
            result.add(obj);
            return result;
        }
    }

    public static int fibonacci(int n) {//3
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        if (n < 0) {
            return -1;//caso excepcional cuando desde el ingreso tiene valore sno validos
        }

        return fibonacci(n - 1) + fibonacci(n - 2);
    }

    public static int sumaNaturales(int n) {  // 4    
        if (n == 1) {
            return 1;
        }
        if (n < 1) {
            return -1;//caso excepcional cuando desde el ingreso tiene valore sno validos
        }
        return n + sumaNaturales(n - 1);
    }

    /**
     * imprime los numeros Naturales entre a y d a debe ser menor que d
     *
     * @param a
     * @param d
     */
    public static void imprimirNaturales(int a, int d) {//5

        if (d < a) {
            System.out.println("a debe ser menor que d");//caso excepcional cuando desde el ingreso tiene valore sno validos
        } else if (d == a) {
            System.out.print(" "+a);
        } else {

            imprimirNaturales(a, d - 1);
            System.out.print(" "+d);
        }
    }

    public static int contarDigitos(int n) { //6      
        if (n < 10) {
            return 1;
        }

        return 1 + contarDigitos(n / 10);
    }

    public static int XeYMultiplicaciones(int x, int y) { //6 
        if (y == 0) {
            return 1;
        }
        if (y == 1) {
            return x;
        }

        return x * XeYMultiplicaciones(x, y - 1);
    }

    public static int XxYsumas(int x, int y) { //6      
        if (x == 0 || y == 0) {
            return 0;
        }
        if (y == 1) {
            return x;
        }
        return x + XxYsumas(x, y - 1);
    }

}
